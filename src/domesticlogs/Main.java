/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package domesticlogs;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;
/**
 *
 * @author Prashant
 */
public class Main extends Application {

    @Override
    public void start(Stage stage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("/domesticlogs/view/StartPage.fxml"));

        	stage.getIcons().add(new Image(Main.class.getResourceAsStream("/domesticlogs/appicon.png")));
 //        Screen screen = Screen.getPrimary();
//        Rectangle2D bounds = screen.getVisualBounds();
//
////        stage.setX(bounds.getMinX());
////        stage.setY(bounds.getMinY());
////        stage.setWidth(bounds.getWidth());
////        stage.setHeight(bounds.getHeight());
        stage.sizeToScene();
        Scene scene = new Scene(root);
        stage.setTitle("Domestic Logs");
        stage.setScene(scene);
        stage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }

}
