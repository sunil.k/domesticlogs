/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package domesticlogs.controller;

import java.net.URL;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ResourceBundle;

import org.controlsfx.control.textfield.TextFields;

import domesticlogs.dao.LogDao;
import domesticlogs.pojo.LogModel;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import javafx.util.StringConverter;

/**
 *
 * @author Prashant
 */
public class NewLogController implements Initializable {

	@FXML
	private TextField particularsTextField;
	@FXML
	private TextField quantityTextField;
	@FXML
	private TextField amountTextField;
	@FXML
	private TextField commentTextField;
	@FXML
	private Button saveLogButton;
	@FXML
	private Button cacleSaveLog;
	@FXML
	private DatePicker datePicker;
	private ComboBox<String> selectLogCombobox;
	private final String pattern = "dd-MMM-yyyy";
	private final DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern(pattern);

	@Override
	public void initialize(URL url, ResourceBundle rb) {
		datePicker.setConverter(new StringConverter<LocalDate>() {

			{
				// datePicker.setPromptText(pattern.toLowerCase());
				datePicker.setValue(LocalDate.now());
				datePicker.setEditable(false);
			}

			@Override
			public String toString(LocalDate date) {
				if (date != null) {
					return dateFormatter.format(date);
				} else {
					return "";
				}
			}

			@Override
			public LocalDate fromString(String string) {
				if (string != null && !string.isEmpty()) {
					return LocalDate.parse(string, dateFormatter);
				} else {
					return null;
				}
			}
		});
//		TextFields.bindAutoCompletion(particularsTextField, new LogDao().findParticularList());

	}

	@FXML
	private void OnSaveLog() {
		if (particularsTextField.getText().compareTo("") == 0 || quantityTextField.getText().compareTo("") == 0
				|| amountTextField.getText().compareTo("") == 0) {
			new Alert(AlertType.ERROR, "Fields can not be empty, except comment!", ButtonType.OK).showAndWait();
//			System.out.println("If executed");

		} else {
//			System.out.println("else executed");
			LogModel newLog = new LogModel();
			newLog.setParticulars(particularsTextField.getText());
			newLog.setQuantity(quantityTextField.getText());
			newLog.setAmount(Double.parseDouble(amountTextField.getText()));
			newLog.setLogType(selectLogCombobox.getValue());
			newLog.setDate(datePicker.getValue().format(dateFormatter));
			newLog.setComment(commentTextField.getText());
			new LogDao().create(newLog);
			new Alert(AlertType.INFORMATION, "Log " + selectLogCombobox.getValue() + " was created with success!",
					ButtonType.OK).showAndWait();

		}

	}

	public void setSelectLogCombobox(ComboBox<String> selectLogCombobox) {
		TextFields.bindAutoCompletion(particularsTextField,
				new LogDao().findParticularList(selectLogCombobox.getValue()));
		this.selectLogCombobox = selectLogCombobox;
	}

	@FXML
	private void OnCancleSaveLog() {
		Stage stage = (Stage) cacleSaveLog.getScene().getWindow();
		stage.close();
	}

}
