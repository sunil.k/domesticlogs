/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package domesticlogs.controller;

import java.net.URL;
import java.util.Arrays;
import java.util.List;
import java.util.ResourceBundle;

import domesticlogs.EnumServices.FilterLogs;
import domesticlogs.dao.LogDao;
import domesticlogs.pojo.LogModel;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.MenuItem;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableRow;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseButton;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author Prashant
 */
public class HomePageController implements Initializable {

	@FXML
	private ComboBox<String> selectLogCombobox;
	@FXML
	private TableView<LogModel> logTableView;
	@FXML
	private TableColumn<LogModel, String> particulars;
	@FXML
	private TableColumn<LogModel, Integer> sNo;
	@FXML
	private TableColumn<LogModel, String> quantity;
	@FXML
	private TableColumn<LogModel, Double> amount;
	@FXML
	private TableColumn<LogModel, String> date;
	@FXML
	private TableColumn<LogModel, String> comment;
	@FXML
	private ImageView loader;
	@FXML
	private Button newLogButton;
	@FXML
	private TextField filterField;
	@FXML
	private ToggleGroup customMenuItemToggleGroup;
	@FXML
	private RadioButton lastOneMonthRadiobtn;
	@FXML
	private RadioButton lastThreeMonthRadiobtn;
	@FXML
	private RadioButton lastSixMonthRadiobtn;
	@FXML
	private RadioButton lastOneYearRadiobtn;
	@FXML
	private RadioButton customDateChoiceRadiobtn;
	@FXML
	private RadioButton noFilterRadiobtn;

//	@FXML
//	private Button deleteLogButton;
//	@FXML
//	private Button editLogButton;

	private final ContextMenu CONTEXT_MENU_TABLE_ROW = new ContextMenu();
	private final MenuItem EDIT_TABLE_ROW = new MenuItem("Edit Log");
	private final MenuItem DELETE_TABLE_ROW = new MenuItem("Delete Log");
	private static FilterLogs filterLogs;
	private final static LogDao logDao = new LogDao();

	/**
	 * Initializes the controller class.
	 */
	@Override
	public void initialize(URL url, ResourceBundle rb) {

		lastOneMonthRadiobtn.setUserData(FilterLogs.LAST_ONE_MONTH);
		lastThreeMonthRadiobtn.setUserData(FilterLogs.LAST_THREE_MONTH);
		lastSixMonthRadiobtn.setUserData(FilterLogs.LAST_ONE_MONTH);
		lastOneYearRadiobtn.setUserData(FilterLogs.LAST_ONE_YEAR);
		customDateChoiceRadiobtn.setUserData(FilterLogs.CUSTOM_DATE_CHOICE);
		noFilterRadiobtn.setUserData(FilterLogs.NO_FILTER);
		final List<RadioButton> radioButtonList = Arrays.asList(noFilterRadiobtn, lastOneMonthRadiobtn,
				lastThreeMonthRadiobtn, lastSixMonthRadiobtn, lastOneYearRadiobtn, customDateChoiceRadiobtn);
		logTableView.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
		selectLogCombobox.getItems().addAll("All logs", "Grocery", "Vegetables", "Phone Recharge",
				"Journey Expenditure", "Medicines", "Room Rent", "Electricity Bill", "Others");
		selectLogCombobox.getSelectionModel().selectFirst();
		populateDataInTable();
		CONTEXT_MENU_TABLE_ROW.getItems().addAll(EDIT_TABLE_ROW, DELETE_TABLE_ROW);
		logTableView.setRowFactory(tv -> {
			TableRow<LogModel> row = new TableRow<>();
			row.setOnMouseClicked(clickEvent -> {
				if (CONTEXT_MENU_TABLE_ROW.isShowing()) {
					CONTEXT_MENU_TABLE_ROW.hide();
				}
				if ((clickEvent.getButton() == MouseButton.SECONDARY) && (!row.isEmpty())) {
					CONTEXT_MENU_TABLE_ROW.show(logTableView, clickEvent.getScreenX(), clickEvent.getScreenY());
				}
			});
			return row;
		});

		radioButtonList.forEach(rbtn -> {
			rbtn.setOnAction(e -> {
				populateDataInTable();
			});
		});

	}

	@FXML
	private void OnSelectLog() {
		populateDataInTable();
	}

	private void populateDataInTable() {
		ObservableList<LogModel> list = FXCollections.observableArrayList();
		loader.setVisible(true);
		Thread thread = new Thread(() -> {
			filterLogs = (FilterLogs) customMenuItemToggleGroup.getSelectedToggle().getUserData();
			switch (filterLogs) {
			case LAST_ONE_MONTH:
//				System.out.println("Here");
//				LocalDate now = LocalDate.now();
//				LocalDate earlier = now.minusMonths(1);
				list.addAll(logDao.findAsPerFilter(selectLogCombobox.getValue(), 30));
				break;
			case LAST_THREE_MONTH:
				list.addAll(logDao.findAsPerFilter(selectLogCombobox.getValue(), 90));
				break;
			case LAST_SIX_MONTH:
				list.addAll(logDao.findAsPerFilter(selectLogCombobox.getValue(), 180));
				break;
			case LAST_ONE_YEAR:
				list.addAll(logDao.findAsPerFilter(selectLogCombobox.getValue(), 365));
				break;
			case CUSTOM_DATE_CHOICE:
				break;
			default:
				list.addAll(logDao.find(selectLogCombobox.getValue()));
			}
			sNo.setCellValueFactory(new PropertyValueFactory<>("id"));
			particulars.setCellValueFactory(new PropertyValueFactory<>("particulars"));
			quantity.setCellValueFactory(new PropertyValueFactory<>("quantity"));
			amount.setCellValueFactory(new PropertyValueFactory<>("amount"));
			date.setCellValueFactory(new PropertyValueFactory<>("date"));
			comment.setCellValueFactory(new PropertyValueFactory<>("comment"));
			// logTableView.getItems().clear();
			FilteredList<LogModel> filteredData = new FilteredList<>(list, p -> true);
			filterField.textProperty().addListener((observable, oldValue, newValue) -> {
				filteredData.setPredicate(model -> {
					// If filter text is empty, display all persons.
					if (newValue == null || newValue.isEmpty()) {
						return true;
					}

					// Compare first name and last name of every person with filter text.
					String lowerCaseFilter = newValue.toLowerCase();

					if (model.getParticulars().toLowerCase().contains(lowerCaseFilter)) {
						return true; // Filter matches first name.
					}
					return false; // Does not match.
				});
			});

			SortedList<LogModel> sortedData = new SortedList<>(filteredData);
			sortedData.comparatorProperty().bind(logTableView.comparatorProperty());
			logTableView.setItems(sortedData);
			loader.setVisible(false);

		});
		thread.setDaemon(true);
		thread.start();
	}

	@FXML
	private void OnNewLog() {
		if (selectLogCombobox.getValue().equalsIgnoreCase("all logs")) {
			new Alert(AlertType.ERROR, "Please select valid log type, All logs is invalid!", ButtonType.OK)
					.showAndWait();

		} else {
			try {
				FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/domesticlogs/view/NewLog.fxml"));
				Parent root = (Parent) fxmlLoader.load();
				Stage stage = new Stage();
				stage.setTitle("Add Log " + selectLogCombobox.getValue());
				stage.setScene(new Scene(root));
				stage.show();
				NewLogController newLogController = (NewLogController) fxmlLoader.getController();
				newLogController.setSelectLogCombobox(selectLogCombobox);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

	}

//	@FXML
//	private void OnDeleteLog() {
//
//	}
//
//	@FXML
//	private void OnEditLog() {
//
//	}

}
