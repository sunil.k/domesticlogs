/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package domesticlogs.pojo;

import java.time.LocalDate;

/**
 *
 * @author Prashant
 */
public class LogModel {

    private int id;
    private String particulars;
    private String quantity;
    private double amount;
    private String date;
    private String logType;
    private String comment;

    public LogModel() {
    }

    public LogModel(int id, String particulars, String quantity, double amount, String date, String comment) {
        this.id = id;
        this.particulars = particulars;
        this.quantity = quantity;
        this.amount = amount;
        this.date = date;
        this.comment = comment;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getParticulars() {
        return particulars;
    }

    public void setParticulars(String particulars) {
        this.particulars = particulars;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getLogType() {
        return logType;
    }

    public void setLogType(String logType) {
        this.logType = logType;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    @Override
    public int hashCode() {
        return id;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final LogModel other = (LogModel) obj;
        if (this.id != other.id) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "LogModel{" + "id=" + id + ", particulars=" + particulars + ", quantity=" + quantity + ", amount=" + amount + ", date=" + date + ", logType=" + logType + ", comment=" + comment + '}';
    }

}
