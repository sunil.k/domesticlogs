package domesticlogs.dao;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
public class DateExample {
	public static void main(String[] args) {
		LocalDate now = LocalDate.now(); 
		LocalDate earlier = now.minusMonths(1); 
		
		System.out.println("earlier.getMonth() = "+DateTimeFormatter.ofPattern("dd-MMM-yyyy").format(earlier));
		System.out.println("now.getMonth()) = "+DateTimeFormatter.ofPattern("dd-MMM-yyyy").format(now));
	}

}
