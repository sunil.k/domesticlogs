/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package domesticlogs.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import domesticlogs.DBUtils;
import domesticlogs.pojo.LogModel;

/**
 *
 * @author Prashant
 */
public class LogDao {

	public void create(LogModel newLog) {
		Connection conn = DBUtils.getConnection();
		try {
//			DateTimeFormatter FOMATTER = DateTimeFormatter.ofPattern("dd-MMM-yyyy");

			// Local date instance
//			LocalDate localDate = LocalDate.now();
//
//			// Get formatted String
//			String dateString = FOMATTER.format(localDate);
			String sql = "insert into domestic_logs_data (particulars,quantity,amount,date,log_type,comment) values(?,?,?,?,?,?)";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, newLog.getParticulars());
			ps.setString(2, newLog.getQuantity());
			ps.setDouble(3, newLog.getAmount());
			ps.setString(4, newLog.getDate());
			ps.setString(5, newLog.getLogType());
			ps.setString(6, newLog.getComment());
			ps.executeUpdate();
			ps.close();
			conn.close();
		} catch (SQLException sq) {
			System.out.println("Unable to create a new row." + sq);
		}
	}

	public List<LogModel> find(String logType) {
		List<LogModel> list = new ArrayList<>();
		try {
			Connection conn = DBUtils.getConnection();
			ResultSet rs = null;
			PreparedStatement stmt = null;
			if (logType.equalsIgnoreCase("all logs")) {
				stmt = conn.prepareStatement("select * from domestic_logs_data order by id desc");
				rs = stmt.executeQuery();
			} else {
				stmt = conn.prepareStatement("select * from domestic_logs_data where log_type = ?");
				stmt.setString(1, logType);
				rs = stmt.executeQuery();
			}
			int i = 1;
			while (rs.next()) {
				list.add(new LogModel(i, rs.getString("particulars"), rs.getString("quantity"), rs.getDouble("amount"),
						rs.getString("date"), rs.getString("comment")));
				i++;
			}
			rs.close();
			stmt.close();
			conn.close();

		} catch (Exception e) {
			System.out.println(e.getClass().getName() + ": " + e.getMessage());
		}
		return list;

	}

	public List<LogModel> findAsPerFilter(String logType, int limit) {
		List<LogModel> list = new ArrayList<>();
		try {
			Connection conn = DBUtils.getConnection();
			ResultSet rs = null;
			PreparedStatement stmt = null;
			if (logType.equalsIgnoreCase("all logs")) {
//				stmt = conn.prepareStatement(
//						"SELECT * FROM domestic_logs_data  WHERE date BETWEEN ? AND ?");
				stmt = conn.prepareStatement("SELECT * FROM domestic_logs_data LIMIT ?;");
				stmt.setInt(1, limit);
				// stmt.setString(1, strDate);
				// stmt.setString(2, endDate);
				rs = stmt.executeQuery();
			} else {
				stmt = conn.prepareStatement("SELECT * FROM domestic_logs_data WHERE log_type = ? LIMIT ? ");
				stmt.setString(1, logType);
				stmt.setInt(2, limit);
				rs = stmt.executeQuery();
			}
			int i = 1;
			while (rs.next()) {
				list.add(new LogModel(i, rs.getString("particulars"), rs.getString("quantity"), rs.getDouble("amount"),
						rs.getString("date"), rs.getString("comment")));
				i++;
			}
			rs.close();
			stmt.close();
			conn.close();

		} catch (Exception e) {
			System.out.println(e.getClass().getName() + ": " + e.getMessage());
		}
		return list;

	}

	public List<String> findParticularList(String logType) {
		List<String> list = new ArrayList<>();
		try {
			Connection conn = DBUtils.getConnection();
			ResultSet rs = null;
			PreparedStatement stmt = conn
					.prepareStatement("select distinct particulars from domestic_logs_data where log_type = ?");
			stmt.setString(1, logType);
			rs = stmt.executeQuery();
			while (rs.next()) {
				list.add(rs.getString("particulars"));
			}
			rs.close();
			stmt.close();
			conn.close();
		} catch (Exception e) {
			System.out.println(e.getClass().getName() + ": " + e.getMessage());
		}

		return list;

	}

//    public void edit(Products products) {
//        Connection conn = MyDatabaseConnection.getConnection();
//        try {
//            String sql = "update products set categoryid = ?, productname = ?, productprice = ?, stockinhand = ? , dangerlevel = ? where productid = ?";
//            PreparedStatement ps = conn.prepareStatement(sql);
//            ps.setInt(1, products.getCategoryId());
//            ps.setString(2, products.getProductName());
//            ps.setFloat(3, products.getProductprice());
//            ps.setInt(4, products.getStockInHand());
//            ps.setInt(5, products.getDangerLevel());
//            ps.setInt(6, products.getProductId());
//            ps.executeUpdate();
//        } catch (SQLException sq) {
//            System.out.println("Unable to create a new row." + sq);
//        }
//    }
//
//    public void UpdateStock(int newStock, int productId) {
//        Connection conn = MyDatabaseConnection.getConnection();
//        try {
//            String sql = "update products set stockinhand = ? where productid = ?";
//            PreparedStatement ps = conn.prepareStatement(sql);
//            ps.setInt(1, newStock);
//            ps.setInt(2, productId);
//            ps.executeUpdate();
//        } catch (SQLException sq) {
//            System.out.println("Unable to create a new row." + sq);
//        }
//    }
//
//    public void edit2(Products products) {
//        Connection conn = MyDatabaseConnection.getConnection();
//        try {
//            String sql = "update products set  productprice = ?, stockinhand = ? , dangerlevel = ? where productid = ?";
//            PreparedStatement ps = conn.prepareStatement(sql);
//            ps.setFloat(1, products.getProductprice());
//            ps.setInt(2, products.getStockInHand());
//            ps.setInt(3, products.getDangerLevel());
//            ps.setInt(4, products.getProductId());
//            ps.executeUpdate();
//        } catch (SQLException sq) {
//            System.out.println("Unable to create a new row." + sq);
//        }
//    }
//
//    public void remove(int productId) {
//        Connection conn = MyDatabaseConnection.getConnection();
//        try {
//            String sql = "delete from products where productid = ?";
//            PreparedStatement ps = conn.prepareStatement(sql);
//            ps.setInt(1, productId);
//            ps.executeUpdate();
//        } catch (SQLException sq) {
//            System.out.println("Unable to create a new row." + sq);
//        }
//    }
//
//    public Products find(int productId) {
//        Connection conn = MyDatabaseConnection.getConnection();
//        Products products = new Products();
//        try {
//            String sql = "select * from products where productid = ?";
//            PreparedStatement ps = conn.prepareStatement(sql);
//            ps.setInt(1, productId);
//            ResultSet rs = ps.executeQuery();
//            if (rs.next()) {
//                products.setProductId(productId);
//                products.setCategoryId(rs.getInt("categoryid"));
//                products.setProductName(rs.getString("productname"));
//                products.setProductprice(rs.getFloat("productprice"));
//                products.setStockInHand(rs.getInt("stockinhand"));
//                products.setDangerLevel(rs.getInt("dangerlevel"));
//            }
//        } catch (SQLException sq) {
//            System.out.println("Unable to create a new row." + sq);
//        }
//        return products;
//    }
//
//    public ArrayList<Products> findAll() {
//        Connection conn = MyDatabaseConnection.getConnection();
//        ArrayList<Products> listProducts = new ArrayList<Products>();
//        try {
//            String sql = "select * from Products";
//            PreparedStatement ps = conn.prepareStatement(sql);
//            ResultSet rs = ps.executeQuery();
//            while (rs.next()) {
//                Products products = new Products();
//                products.setProductId(rs.getInt("productId"));
//                products.setCategoryId(rs.getInt("categoryid"));
//                products.setProductName(rs.getString("productname"));
//                products.setProductprice(rs.getFloat("productprice"));
//                products.setStockInHand(rs.getInt("stockinhand"));
//                products.setDangerLevel(rs.getInt("dangerlevel"));
//                listProducts.add(products);
//            }
//        } catch (SQLException sq) {
//            System.out.println("Unable to create a new row.");
//        }
//        return listProducts;
//    }
//
//    public ArrayList<Integer> findAllIds() {
//        Connection conn = MyDatabaseConnection.getConnection();
//        ArrayList<Integer> listIds = new ArrayList<Integer>();
//        try {
//            String sql = "select productid from products";
//            PreparedStatement ps = conn.prepareStatement(sql);
//            ResultSet rs = ps.executeQuery();
//            while (rs.next()) {
//                listIds.add(rs.getInt("productid"));
//            }
//        } catch (SQLException sq) {
//            System.out.println("Unable to create a new row.");
//        }
//        return listIds;
//    }
//
//    public ArrayList<String> findAllProductName() {
//        Connection conn = MyDatabaseConnection.getConnection();
//        ArrayList<String> listProductName = new ArrayList<String>();
//        try {
//            String sql = "select productname from Products";
//            PreparedStatement ps = conn.prepareStatement(sql);
//            ResultSet rs = ps.executeQuery();
//            while (rs.next()) {
//                listProductName.add(rs.getString("productname"));
//            }
//        } catch (SQLException sq) {
//            System.out.println("Unable to create a new row.");
//        }
//        return listProductName;
//    }
//
//    public ArrayList<Products> findAllByProductId(int productId) {
//        Connection conn = MyDatabaseConnection.getConnection();
//        ArrayList<Products> listProducts = new ArrayList<Products>();
//        try {
//            String sql = "select * from Products where productid = ?";
//            PreparedStatement ps = conn.prepareStatement(sql);
//            ps.setInt(1, productId);
//            ResultSet rs = ps.executeQuery();
//            while (rs.next()) {
//                Products products = new Products();
//                products.setProductId(productId);
//                products.setCategoryId(rs.getInt("categoryid"));
//                products.setProductName(rs.getString("productname"));
//                products.setProductprice(rs.getFloat("productprice"));
//                products.setStockInHand(rs.getInt("stockinhand"));
//                products.setDangerLevel(rs.getInt("dangerlevel"));
//                listProducts.add(products);
//            }
//        } catch (SQLException sq) {
//            System.out.println("Unable to create a new row." + sq);
//        }
//        return listProducts;
//    }
//
//    public Products findProductPriceNStock(int productId) {
//        Connection conn = MyDatabaseConnection.getConnection();
//        Products products = new Products();
//        try {
//            String sql = "select productprice,stockinhand from Products where productid = ?";
//            PreparedStatement ps = conn.prepareStatement(sql);
//            ps.setInt(1, productId);
//            ResultSet rs = ps.executeQuery();
//            while (rs.next()) {
//
//                products.setProductprice(rs.getFloat("productprice"));
//                products.setStockInHand(rs.getInt("stockinhand"));
//
//            }
//        } catch (SQLException sq) {
//            System.out.println("Unable to create a new row." + sq);
//        }
//        return products;
//
//    }
//
//    public ArrayList<Products> findProductsByCategoryId(int categoryId) {
//
//        Connection conn = MyDatabaseConnection.getConnection();
//        ArrayList<Products> listProducts = new ArrayList<Products>();
//        try {
//            String sql = "select * from Products where categoryid = ?";
//            PreparedStatement ps = conn.prepareStatement(sql);
//            ps.setInt(1, categoryId);
//            ResultSet rs = ps.executeQuery();
//            while (rs.next()) {
//                Products products = new Products();
//                products.setCategoryId(categoryId);
//                products.setProductId(rs.getInt("productid"));
//                products.setProductName(rs.getString("productname"));
//                products.setProductprice(rs.getFloat("productprice"));
//                products.setStockInHand(rs.getInt("stockinhand"));
//                products.setDangerLevel(rs.getInt("dangerlevel"));
//                listProducts.add(products);
//            }
//        } catch (SQLException sq) {
//            System.out.println("Unable to create a new row." + sq);
//        }
//        return listProducts;
//
//    }
//
//    public float getMinPrice() {
//        Connection conn = MyDatabaseConnection.getConnection();
//        float minPrice = 0;
//        try {
//            String sql = "select MIN(productprice) as MinimumPrice from Products ";
//            PreparedStatement ps = conn.prepareStatement(sql);
//            ResultSet rs = ps.executeQuery();
//            while (rs.next()) {
//
//                minPrice = rs.getFloat("MinimumPrice");
//
//            }
//        } catch (SQLException sq) {
//            System.out.println("Unable to create a new row." + sq);
//        }
//        return minPrice;
//
//    }
//
//    public float getMaxPrice() {
//        Connection conn = MyDatabaseConnection.getConnection();
//        float maxPrice = 0;
//        try {
//            String sql = "select Max(productprice) as MaximumPrice from Products ";
//            PreparedStatement ps = conn.prepareStatement(sql);
//            ResultSet rs = ps.executeQuery();
//            while (rs.next()) {
//
//                maxPrice = rs.getFloat("MaximumPrice");
//
//            }
//        } catch (SQLException sq) {
//            System.out.println("Unable to create a new row." + sq);
//        }
//        return maxPrice;
//
//    }
//
//    public ArrayList<Products> findProductsByRange(int minValue, int maxValue) {
//
//        Connection conn = MyDatabaseConnection.getConnection();
//        ArrayList<Products> listProducts = new ArrayList<Products>();
//        try {
//            String sql = "select * from Products where productprice BETWEEN ? AND ? ORDER BY productprice asc";
//            PreparedStatement ps = conn.prepareStatement(sql);
//            ps.setInt(1, minValue);
//            ps.setInt(2, maxValue);
//            ResultSet rs = ps.executeQuery();
//            while (rs.next()) {
//                Products products = new Products();
//                products.setCategoryId(rs.getInt("categoryid"));
//                products.setProductId(rs.getInt("productid"));
//                products.setProductName(rs.getString("productname"));
//                products.setProductprice(rs.getFloat("productprice"));
//                products.setStockInHand(rs.getInt("stockinhand"));
//                products.setDangerLevel(rs.getInt("dangerlevel"));
//                listProducts.add(products);
//            }
//        } catch (SQLException sq) {
//            System.out.println("Unable to create a new row." + sq);
//        }
//        return listProducts;
//
//    }
}
